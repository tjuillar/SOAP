package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.InputMismatchException;

import javax.swing.JFrame;
import javax.xml.rpc.ServiceException;

import fr.uge.IfsCarsService;
import fr.uge.IfsCarsServiceServiceLocator;
import fr.uge.IfsCarsServiceSoapBindingStub;
import fr.uge.observe.IObservateur;
import fr.uge.observe.IObserve;
import fr.uge.observe.Observateur;

public class ClientStub extends JFrame {
	private IfsCarsService ifs;
	private boolean isConnected = false;
	private String login;
	private final List<String> rentBasket;
	private final Map<String, String> rentBasketFront;

	private final List<String> purchaseBasket;
	private final Map<String, String> purchaseBasketFront;
	private final static String problem = "Probleme avec SOAP, ré-essaie plus tard.";
	private IObserve observe;

	public ClientStub() {
		//TMP
		try {
			ifs = new IfsCarsServiceServiceLocator().getIfsCarsService();
			((IfsCarsServiceSoapBindingStub) ifs).setMaintainSession(true);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rentBasket = new ArrayList<String>();
		purchaseBasket = new ArrayList<String>();
		purchaseBasketFront = new HashMap<String, String>();
		rentBasketFront = new HashMap<String, String>();
	}

	public boolean isConnected() {
		return isConnected;
	}
	
	void getCurrentRentals() {
		try {
			System.out.println(ifs.getCurrentRentals(login));
		} catch (RemoteException e) {
			System.out.println("Probl�me SOAP: Les locations n'ont pas pu �tre affich�es.");
		}
	}
	
	String getCurrentRentalsFront() {
		try {
			return ifs.getCurrentRentals(login);
		} catch (RemoteException e) {
			System.out.println("Probl�me SOAP: Les locations n'ont pas pu �tre affich�es.");
		}
		return "";
	}

	/**
	 * Reccupère un int de l'utilisateur
	 * 
	 * @param msg message qui sera affiché
	 * @return
	 */
	public int getInt(String msg) {
		System.out.println(msg);
		Scanner sc = new Scanner(System.in);
		String res = "";
		while(sc.hasNext()) {
			try {
				return Integer.valueOf(sc.nextLine());
			}
			catch(NumberFormatException ime) {
				System.out.println("Veuillez entrer un nombre");
			}
		}
		
		return 0;
		
	}

	/**
	 * Reccupère un String de l'utilisateur
	 * 
	 * @param msg message qui sera affiché
	 * @return
	 */
	public String getString(String msg) {
		System.out.println(msg);
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()) {
			return sc.nextLine();
		}
		throw new IllegalStateException();
	}
	

	void changeCurrency() {
		try {
			String currency = getString("Quelle est la devise que vous souhaitez utiliser ? ");
		
			if (!ifs.changeCurrency(currency)) {
				System.out.println("Cette devise n'est pas prise en compte par l'application. Veuillez réessayer avec une devise correcte.");
			}
			else {
				System.out.println("La nouvelle devise sera " + currency);
			}
		} catch (RemoteException e) {
			System.out.println("Problème lors de l'appel à la fonction du serve IfsCarsService. Veuillez réessayer.");
			return;
		}
	}
	
	

	void connexion() {
		try {
			String login = getString("login : ");
			String password = getString("password : ");

			if (ifs.isEmployee(login, password)) {
				rentBasket.clear();
				purchaseBasket.clear();
				System.out.println(
						"Authentification réussi, vous pouvez désormais acheter certains v�hicules à la location.");
				isConnected = true;
				this.login = login;
			} else {
				System.out.println("Authentification échoué.");
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	boolean connexionFront(String login, String password) {
		try {

			if (ifs.isEmployee(login, password)) {
				rentBasket.clear();
				purchaseBasket.clear();
				System.out.println(
						"Authentification réussi, vous pouvez désormais acheter certains v�hicules à la location.");
				isConnected = true;
				this.login = login;
				return true;
			} else {
				System.out.println("Authentification échoué.");
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	void displayVehicleRented() {
		try {
			String a = "test";
			StringJoiner sj = new StringJoiner("\n===\n");
			String res = ifs.getRentableVehicles();
			for(String line : res.split("%")) {
				String[] infos = line.split(":");
				sj.add("Le v�hicule immatricul� " + infos[0] + " est de la marque " + infos[1] + ". Le type de carburant utilis� par celui-ci est " + infos[2] +
						".\nIl poss�de " + infos[3] + " places assises.\nEst-il actuellement lou� ? " + infos[4] + ".\nA-t-il �t� achet� ? " + infos[5] +
						".\nIl a �t� lou� " + infos[6] + " fois.\nPrix location : " + infos[7] + "\nPrix achat : " + infos[8] +
						"\nAvis utilisateurs :" + ((infos[9].equals(" ") || infos[9].equals(""))? "Pas encore d'avis.":infos[9]));
			}
			System.out.println(sj.toString());			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	String displayVehicleRentedFront() {
		try {
			return ifs.getRentableVehicles();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Ajoute un véhicule au panier
	 * 
	 * @throws RemoteException
	 */
	void addRentBasket() {
		String immat = getString("Quel véhicule voulez-vous ajouter au panier ?");
		// TODO vérifier immat est bien
		if (rentBasket.contains(immat)) {
			System.out.println("Le v�hicule immatricul� " + immat + " est d�j� dans votre panier");
			return;
		}

		try {
			System.out.println();
			int result = ifs.isAvailable(immat);
			switch (result) {
			case -1:
				System.out.println("Une erreur s'est produite, veuillez r�essayer.");
				return;
			case 0:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il n'a pas �t� trouv�");
				return;
			case 3:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il a d�j� �t� achet�");
				return;
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		rentBasket.add(immat);
		System.out.println("Le v�hicule immatricul� " + immat + " a bien �t� ajout� � votre panier");
	}

	/**
	 * Ajoute un véhicule au panier
	 * 
	 * @throws RemoteException
	 */
	int addRentBasketFront(String immat, String infos) {

		if (rentBasketFront.containsKey(immat)) {
			System.out.println("Le v�hicule immatricul� " + immat + " est d�j� dans votre panier");
			return -1;
		}

		try {
			System.out.println();
			int result = ifs.isAvailable(immat);
			switch (result) {
			case -1:
				System.out.println("Une erreur s'est produite, veuillez r�essayer.");
				return -1;
			case 0:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il n'a pas �t� trouv�");
				return 4;
			case 3:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il a d�j� �t� achet�");
				return 3;
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		rentBasketFront.put(immat, infos);
		System.out.println("Le v�hicule immatricul� " + immat + " a bien �t� ajout� � votre panier");
		return 0;
	}

	/**
	 * affiche de panier en version terminale
	 */
	void displayRentBasket() {
		if (rentBasket.isEmpty()) {
			System.out.println("Votre panier est vide");
			return;
		}
		System.out.println("Voici votre panier :");
		for (String v : rentBasket) {
			System.out.println("\t" + v);
		}
	}

	String displayRentBasketFront() {
		if (rentBasketFront.isEmpty()) {
			System.out.println("Votre panier est vide");
			return "";
		}
		StringJoiner sj = new StringJoiner("%");
		for (String v : rentBasketFront.values()) {
			sj.add(v);
		}
		return sj.toString();
	}

	String displayPurchaseBasketFront() {
		if (purchaseBasketFront.isEmpty()) {
			System.out.println("Votre panier est vide");
			return "";
		}
		StringJoiner sj = new StringJoiner("%");
		// System.out.println("Voici votre panier :");
		for (String v : purchaseBasketFront.values()) {
			// String tmp =
			sj.add(v);
		}
		return sj.toString();
	}

	/**
	 * Permet de supprimer un article de notre panier
	 */
	void removeOneRentBasket() {
		System.out.println("Voici votre panier :");
		for (int i = 0; i < rentBasket.size(); i++) {
			System.out.println(i + " : " + rentBasket.get(i));
		}
		int v = getInt("Quel véhicule voulez-vous supprimer ?");

		if (v >= 0 && v < rentBasket.size()) {
			String vSuppr = rentBasket.remove(v);
			System.out.println("vous avez choisi de supprimer : " + vSuppr);
			return;
		}
		System.out.println("Ce véhicule n'existe pas. Abandon ...");
	}

	/**
	 * Permet de supprimer un article de notre panier
	 */
	String removeOneRentBasketFront(String immat) {
		// System.out.println();
		System.out.println("size basket rent : " + rentBasketFront.size());
		return rentBasketFront.remove(immat);
	}

	String removeOnePurchaseBasketFront(String immat) {
		System.out.println("size basket purchase : " + purchaseBasketFront.size());

		return purchaseBasketFront.remove(immat);
	}

	void returnVehicle() {
		try {
			String immat = getString("Entrez la plaque d'immatriculation du v�hicule que vous souhaitez retourner");
			
			String choice = getString("Souhaitez-vous laisser un feedback ? (oui ou non)");
			if (choice.equals("oui")) {
				int grade = getInt("Quelle note donneriez-vous � votre location (entre 0 et 10)?");
				while (grade < 0 || grade > 10) {
					grade = getInt("Veuillez donner une note entre 0 et 10.");
				}
				String comment = getString("Veuillez inscrire le commentaire qui accompagnera la note donn�e.");
				System.out.println("Envoie de votre restitution");
				ifs.returnVehicleWithFeedback(login, immat, grade, comment);
			} else if (choice.equals("non")) {
				System.out.println("Envoie de votre restitution");
				ifs.returnVehicleWithoutFeedback(login, immat);
			} else {
				System.out.println("Commande inconnue. R�essayez.");
				return;
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Une erreur lors de la restitution du v�hicule s'est produite. Veuillez r�essayer.");
		}
	}
	
	int returnVehicle(String immat, int grade, String comment) {
		try {
			if (grade != -1 && !comment.equals("")) {
				
				ifs.returnVehicleWithFeedback(login, immat, grade, comment);
			} else  {
				System.out.println("Envoie de votre restitution");
				ifs.returnVehicleWithoutFeedback(login, immat);
			}
			return 1;
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Une erreur lors de la restitution du v�hicule s'est produite. Veuillez r�essayer.");
		}
		return -1;
	}

	/**
	 * Tente d'acheter le panier
	 * 
	 * @throws RemoteException
	 */
	void validateRentBasket() {
		try {
			displayRentBasket();
			String response = getString("Voulez-vous valider votre panier ? ('yes' / 'no')");
			if (response.equals("yes")) {
				String account = getString("Veuillez vous identifier � votre compte bancaire:\nLogin:");
				String password = getString("Password:");
				String[] res = new String[rentBasket.size()];
				for (int i = 0; i < rentBasket.size(); i++) {
					res[i] = rentBasket.get(i);
				}
				int[] results = ifs.payRentBasket(res, account, login, password);
				for (int i = 0; i < results.length; i++) {
					switch (results[i]) {
					case -4:
						System.out.println("Les identifiants transmis ne correspondent � aucun compte.");
						return;
					case -3:
						System.out.println("Les identifiants envoy�s ne correspondent pas aux standards de la banque.");
						return;
					case -2:
						System.out.println("Erreur de connexion avec la base de donnée, revenez plus tard.");
						return;
					case -1:
						System.out.println("Un des véhicules n'existe pas ou n'est pas disponible.");
						return;
					case 0:
						System.out.println("Vous n'avez pas assez d'argent sur votre compte.");
						return;
					case 1:
						System.out.println("Le payement de la location du v�hicule immatricul� " + rentBasket.get(i)
								+ " s'est bien pass�, vous pouvez profiter du v�hicule d�s maintenant.");
						break;
					case 2:
						System.out.println("Le payement de la location du v�hicule immatricul� " + rentBasket.get(i)
								+ " s'est bien pass� mais vous avez �t� plac� en file d'attente.");
						if (null == observe) {
							String location = ifs.getSubscribe();
							observe = (IObserve) Naming.lookup(location);
						}
						IObservateur obs1 = new Observateur();
						observe.subscribe(obs1, rentBasket.get(i));
						break;

					}
				}
				rentBasket.clear();
				return;
			}
			System.out.println("Abandon de l'achat ...");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

	/**
	 * Affiche sur la console les véhicules disponible à l'achat
	 */
	void displayVehiclePurchase() {
		try {
			StringJoiner sj = new StringJoiner("\n===\n");
			String res = ifs.getPurchasableVehicles();
			for(String line : res.split("%")) {
				String[] infos = line.split(":");
				
				sj.add("Le v�hicule immatricul� " + infos[0] + " est de la marque " + infos[1] + ". Le type de carburant utilis� par celui-ci est " + infos[2] +
						".\nIl poss�de " + infos[3] + " places assises.\nEst-il actuellement lou� ? " + infos[4] + ".\nA-t-il �t� achet� ? " + infos[5] +
						".\nIl a �t� lou� " + infos[6] + " fois.\nPrix location : " + infos[7] + "\nPrix achat : " + infos[8] +
						"\nAvis utilisateurs :" + ((infos[9].equals(" ") || infos[9].equals(""))? "Pas encore d'avis.":infos[9]));
			}
			System.out.println(sj.toString());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	String displayVehiclePurchaseFront() {
		try {
			return ifs.getPurchasableVehicles();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Ajoute un véhicule au panier d'achat
	 * 
	 * @throws RemoteException
	 */
	void addPurchaseBasket() {
		String immat = getString("Quel véhicule voulez-vous ajouter au panier ?");

		if (purchaseBasket.contains(immat)) {
			System.out.println("Le v�hicule immatricul� " + immat + " est d�j� dans votre panier");
			return;
		}

		try {
			System.out.println();
			int result = ifs.isAvailable(immat);
			switch (result) {
			case -1:
				System.out.println("Une erreur s'est produite, veuillez r�essayer.");
				return;
			case 0:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il n'a pas �t� trouv�");
				return;
			case 2:
				System.out.println("Le v�hicule n'est pas encore disponible � l'achat");
				return;
			case 3:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il a d�j� �t� achet�");
				return;
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO vérifier immat est bien
		purchaseBasket.add(immat);
		System.out.println("Le v�hicule immatricul� " + immat + " a bien �t� ajout� � votre panier");
	}

	/**
	 * Ajoute un véhicule au panier d'achat
	 * 
	 * @throws RemoteException
	 */
	int addPurchaseBasketFront(String immat, String line) {

		if (purchaseBasketFront.containsKey(immat)) {
			System.out.println("Le v�hicule immatricul� " + immat + " est d�j� dans votre panier");
			return -2;
		}

		try {
			int result = ifs.isAvailable(immat);
			switch (result) {
			case -1:
				System.out.println("Une erreur s'est produite, veuillez r�essayer.");
				return -1;
			case 0:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il n'a pas �t� trouv�");
				return 4;
			case 2:
				System.out.println("Le v�hicule n'est pas encore disponible � l'achat");
				return 2;
			case 3:
				System.out.println("Le v�hicule n'a pas pu �tre ajout� au panier car il a d�j� �t� achet�");
				return 3;
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO vérifier immat est bien
		purchaseBasketFront.put(immat, line);
		System.out.println("Le v�hicule immatricul� " + immat + " a bien �t� ajout� � votre panier");
		return 0;
	}

	/**
	 * affiche de panier d'achat en version terminale
	 */
	void displayPurchaseBasket() {
		if (purchaseBasket.isEmpty()) {
			System.out.println("Votre panier est vide");
			return;
		}
		System.out.println("Voici votre panier :");
		for (String v : purchaseBasket) {
			System.out.println("\t" + v);
		}
	}

	/**
	 * Permet de supprimer un article de notre panier d'achat
	 */
	void removeOnePurchaseBasket() {
		System.out.println("Voici votre panier :");
		for (int i = 0; i < purchaseBasket.size(); i++) {
			System.out.println(i + " : " + purchaseBasket.get(i));
		}
		int v = getInt("Quel véhicule voulez-vous supprimer ?");

		if (v > 0 && v < purchaseBasket.size()) {
			String vSuppr = purchaseBasket.remove(v);
			System.out.println("vous avez choisi de supprimer : " + vSuppr);
			return;
		}
		System.out.println("Ce véhicule n'existe pas. Abandon ...");
	}

	/**
	 * Tente d'acheter le panier d'achat
	 * 
	 * @throws RemoteException
	 */
	void validatePurchaseBasket() {
		try {
			displayPurchaseBasket();
			String response = getString("Voulez-vous valider votre panier ? ('yes' / 'no')");
			if (response.equals("yes")) {
				String account = getString("Veuillez vous identifier � votre compte bancaire:\nLogin:");
				String password = getString("Password:");
				String[] res = new String[purchaseBasket.size()];
				for (int i = 0; i < purchaseBasket.size(); i++) {
					res[i] = purchaseBasket.get(i);
				}
				int[] results = ifs.payRentBasket(res, account, login, password);
				for (int i = 0; i < results.length; i++) {
					switch (results[i]) {
					case -4:
						System.out.println("Les identifiants transmis ne correspondent � aucun compte.");
						return;
					case -3:
						System.out.println("Les identifiants envoy�s ne correspondent pas aux standards de la banque.");
						return;
					case -2:
						System.out.println("Erreur de connexion avec la base de donnée, revenez plus tard.");
						return;
					case -1:
						System.out.println("Un des véhicules n'existe pas ou n'est pas disponible.");
						return;
					case 0:
						System.out.println("Vous n'avez pas assez d'argent sur votre compte.");
						return;
					case 1:
						System.out.println("Le payement de la location du v�hicule immatricul� " + purchaseBasket.get(i)
								+ " s'est bien pass�, vous pouvez profiter du v�hicule d�s maintenant.");
						break;

					}
				}
			} else {
				System.out.println("Abandon de l'achat ...");
			}

		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return;
	}
	
	/**
	 * Tente d'acheter le panier d'achat
	 * 
	 * @throws RemoteException
	 */
	int validateRentBasketFront(String account, String password , Front front) {
		try {

			String[] res = new String[rentBasketFront.size()];
			int j = 0;
			for (String k : rentBasketFront.keySet()) {
				res[j] = k;
				j += 1;
			}
			int[] results = ifs.payRentBasket(res, account, login, password);
			System.out.println("res ======= " + results.length);
			for (int i = 0; i < results.length; i++) {
				switch (results[i]) {
				case -4:
					System.out.println("Les identifiants transmis ne correspondent � aucun compte.");
					return -4;
				case -3:
					System.out.println("Les identifiants envoy�s ne correspondent pas aux standards de la banque.");
					return -3;
				case -2:
					System.out.println("Erreur de connexion avec la base de donnée, revenez plus tard.");
					return -2;
				case -1:
					System.out.println("Un des véhicules n'existe pas ou n'est pas disponible.");
					return -1;
				case 0:
					System.out.println("Vous n'avez pas assez d'argent sur votre compte.");
					return 0;
				case 1:
					System.out.println("Le payement de la location du v�hicule immatricul� ");	
					break;
				case 2:
					System.out.println("Le payement de la location du v�hicule immatricul� " + rentBasket.get(i)
							+ " s'est bien pass� mais vous avez �t� plac� en file d'attente.");
					if (null == observe) {
						String location = ifs.getSubscribe();
						observe = (IObserve) Naming.lookup(location);
					}
					IObservateur obs1 = new Observateur(front);
					observe.subscribe(obs1, res[i]);
					break;
				}
				
			}

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rentBasketFront.clear();
		return 2;
	}

	/**
	 * Tente d'acheter le panier d'achat
	 * 
	 * @throws RemoteException
	 */
	int validatePurchaseBasketFront(String account, String password ) {
		try {

			
			String[] res = new String[purchaseBasketFront.size()];
			int j = 0;
			for (String k : purchaseBasketFront.keySet()) {
				res[j] = k;
				j += 1;
			}
			int[] results = ifs.payPurchaseBasket(res, account, login, password);
			for (int i = 0; i < results.length; i++) {
				switch (results[i]) {
				case -4:
					System.out.println("Les identifiants transmis ne correspondent � aucun compte.");
					return -4;
				case -3:
					System.out.println("Les identifiants envoy�s ne correspondent pas aux standards de la banque.");
					return -3;
				case -2:
					System.out.println("Erreur de connexion avec la base de donnée, revenez plus tard.");
					return -2;
				case -1:
					System.out.println("Un des véhicules n'existe pas ou n'est pas disponible.");
					return -1;
				case 0:
					System.out.println("Vous n'avez pas assez d'argent sur votre compte.");
					return 0;
				case 1:
					System.out.println("Le payement de la location du v�hicule.");
					break;

				}
			}

		} catch (RemoteException e) {
			e.printStackTrace();
		}
		purchaseBasketFront.clear();
		return 2;
	}

	void displayComments() {
		try {
			String immat = getString(
					"Vous voulez voir les commentaires de quel véhicule ? (plaque d'immatriculation) : ");
			System.out.println(ifs.getComments(immat));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	String displayCommentsFront(String immat) {
        try {
            String[] infos = ifs.getVehicleInfosTab(immat);

            return "Le véhicule immatriculé " + infos[0] + " est de la marque " + infos[1]
                    + ". Le type de carburant utilisé par celui-ci est " + infos[2] + ".\nIl possède " + infos[3]
                    + " places assises.\nEst-il actuellement loué ? " + infos[4] + ".\nA-t-il été acheté ? " + infos[5]
                    + ".\nIl a été loué " + infos[6] + " fois.\nPrix location : " + infos[7] + "\nPrix achat : "
                    + infos[8] + "\nAvis utilisateurs :"
                    + ((infos[9].equals(" ") || infos[9].equals("")) ? " Pas encore d'avis." : infos[9]);

        } catch (RemoteException e) {
            return "";
        }
    }
	
	boolean changeCurrencyFront(String currency) {
        try {    
            return ifs.changeCurrency(currency);
            
        } catch (RemoteException e) {
            System.out.println("Problème lors de l'appel à la fonction du serve IfsCarsService. Veuillez réessayer.");
            return false;
        }
    }

	void menu() {
		boolean exit = false;
		String message = "\n---------------------------------------------------\r\n" + "0  : Quitter l'application\r\n"
				+ "1  : Se connecter � l'application\r\n" + "2  : Acheter un v�hicule\r\n"
				+ "3  : Louer un v�hicule\r\n" + "---------------------------------------------------";
		while (!exit) {
			switch (getInt(message)) {
			case 0: // Quitter l'application
				exit = true;
				break;
			case 1: // Se connecter � l'application
				connexion();
				break;
			case 2: // Acheter un vehicule
				purchaseMenu();
				break;
			case 3: // Louer un v�hicule
				if (isConnected) {
					rentMenu();
					break;
				} else {
					System.out.println("Merci de vous connecter avant d'accéder à ce menu.");
					break;
				}
			default:
				System.out.println("Cette commande n'est pas reconnue.");
				break;
			}
		}
	}

	void purchaseMenu() {
		boolean exit = false;
		String message = "\n---------------------------------------------------\r\n" + "0  : Quitter l'application\r\n"
				+ "1  : Se connecter à l'application\r\n" + "2  : Afficher les véhicules disponibles à l'achat\r\n"
				+ "3  : Afficher les avis d'un véhicule\r\n" + "4  : Afficher votre panier\r\n"
				+ "5  : Ajouter à votre panier un véhicule à acheter\r\n" + "6  : Gérer votre panier\r\n"
				+ "7  : Valider votre panier d'achat de véhicules\r\n"
				+ "8  : Changer la devise\r\n"
				+ "---------------------------------------------------";
		while (!exit) {
			switch (getInt(message)) {
			case 0: // Quitter l'application
				exit = true;
				break;
			case 1: // Se connecter � l'application
				connexion();
				break;
			case 2: // Afficher les v�hicules disponibles � l'achat
				displayVehiclePurchase();
				break;
			case 3: // afficher les avis d'un véhicule
				displayComments();
				break;
			case 4: // Afficher votre panier
				displayPurchaseBasket();
				break;
			case 5: // Ajouter � votre panier un v�hicule � acheter
				addPurchaseBasket();
				break;
			case 6: // G�rer votre panier
				removeOnePurchaseBasket();
				break;
			case 7: // Valider votre panier d'achat de v�hicules
				validatePurchaseBasket();
				break;
			case 8:
				changeCurrency();
				break;
			}
		}
	}

	void rentMenu() {
		boolean exit = false;
		String message = "\n---------------------------------------------------\r\n" + "0  : Quitter l'application\r\n"
				+ "1  : Afficher les véhicules disponibles à la location\r\n"
				+ "2  : Afficher les avis d'un véhicule\r\n" + "3  : Afficher votre panier\r\n"
				+ "4  : Ajouter à votre panier un véhicule à louer\r\n" + "5  : Gérer votre panier\r\n"
				+ "6  : Valider votre panier de location de véhicules\r\n"
				+ "7  : Rendre votre véhicule en cours de location\r\n"
				+ "8  : Voir tous vos vehicules en cours de location\r\n"
				+ "9  : Changer la devise\r\n"
				+ "---------------------------------------------------";
		while (!exit) {
			switch (getInt(message)) {
			case 0: // Quitter l'application
				exit = true;
				break;
			case 1: // Afficher les v�hicules disponibles � la location
				displayVehicleRented();
				break;
			case 2: // afficher les avis d'un véhicule
				displayComments();
				break;
			case 3: // Afficher votre panier
				displayRentBasket();
				break;
			case 4: // Ajouter � votre panier un v�hicule � louer
				addRentBasket();
				break;
			case 5: // G�rer votre panier
				removeOneRentBasket();
				break;
			case 6: // Valider votre panier de location de v�hicules
				validateRentBasket();
				break;
			case 7: // Rendre votre v�hicule en cours de location
				returnVehicle();
				break;
			case 8: // Voir les vehicules en cours de location
				getCurrentRentals();
				break;
			case 9:
				changeCurrency();
				break;
			}
		}
	}

	public static void main(String[] args) {
		ClientStub client = new ClientStub();
		client.menu();

	}
}
