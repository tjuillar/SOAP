package client;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class Front extends JFrame {

	private final ClientStub clientStub = new ClientStub();
	private static final long serialVersionUID = -8422323715771827020L;

	private static JPanel mainPanel;
	private static JPanel centerPanel;
	private static JPanel purchasePanelMenu;
	private static JPanel purchasePanelCenter;
	private static JPanel rentPanelMenu;
	private static JPanel rentPanelCenter;
	private JComboBox jcb = new JComboBox();

	// bouton MenuLayout

	public Front() {
		super("Eiffel Corp App ");

		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	   

		this.setSize(800, 600);
		this.setLocationRelativeTo(null);
	}

	// Toolbar

	public void callNotify(String s) {
		int addVehicleBasket = JOptionPane.showConfirmDialog(mainPanel, s, "VOUS avez une notification",
				JOptionPane.CLOSED_OPTION);
	}

	public static void onClickedBtnQuit() {
		System.out.println("L'application a Ã©tÃ© fermÃ©");
		System.exit(0);
	}
// ------- Purchase ----------

	/**
	 * Initialise la toolbar
	 * 
	 * @param toolbar
	 */
	private void createMenu(JToolBar toolbar) {
		JButton btnQuit = new JButton("Quitter");
		JButton btnConnex = new JButton("Se connecter");
		JButton btnMenuPurchase = new JButton("Menu Achat");
		JButton btnMenuRent = new JButton("Menu Location");

		// Pour la partie popUp connexion (login + password)
		JLabel jlblUsername = new JLabel("Username");
		JLabel jlblPassword = new JLabel("Password");
		JTextField jtfUsername = new JTextField(15);
		JPasswordField jpfPassword = new JPasswordField();

		JPanel p3 = new JPanel(new GridLayout(2, 1));
		p3.add(jlblUsername);
		p3.add(jlblPassword);

		// On rajoute les champs TextField : login et mdp
		JPanel p4 = new JPanel(new GridLayout(2, 1));
		p4.add(jtfUsername);
		p4.add(jpfPassword);

		// On ajoute p3 et p4 pour crÃ©er la zone de Pop Up
		JPanel p1 = new JPanel();
		p1.add(p3);
		p1.add(p4);

		btnConnex.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				int connexionPopUp = JOptionPane.showConfirmDialog(mainPanel, p1, "Connexion",
						JOptionPane.OK_CANCEL_OPTION);
				if (connexionPopUp == JOptionPane.OK_OPTION) {
					// TODO
					System.out.println("Yessss");
					System.out.println(jtfUsername.getText());
					String nameSaisie = jtfUsername.getText();
					System.out.println(jpfPassword.getPassword());
					if (clientStub.connexionFront(jtfUsername.getText(), String.valueOf(jpfPassword.getPassword()))) {
						System.out.println("Connexion � l'application avec l'utilisateur : " + jtfUsername);
					} else {
						System.out.println("La connexion � l'application a �chou� : Mauvais login ou MDP ");
					}

				} else {
					System.out.println("Connexion annul�");
				}

			}
		});

		btnQuit.addActionListener(e -> onClickedBtnQuit());
		btnMenuPurchase.addActionListener(e -> {
			System.out.println("Appel du Menu d'achat");
			centerPanel.setVisible(false);
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout());
			centerPanel.add(new Button("PURCHASE"));
			JPanel tmp = createPurchasePanelMenu();
			centerPanel.add(tmp, BorderLayout.WEST);
			mainPanel.add(centerPanel, BorderLayout.CENTER);

			this.repaint();
			this.revalidate();
		});

		btnMenuRent.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				if (!clientStub.isConnected()) {
					int connexionPopUp = JOptionPane.showConfirmDialog(mainPanel,
							"Connectez-vous pour accèdez aux locations", "WARNING", JOptionPane.CLOSED_OPTION);
					return;
				} else {
					System.out.println("Appel du Menu d'rent");
					centerPanel.setVisible(false);
					centerPanel = new JPanel();
					centerPanel.setLayout(new BorderLayout());
					centerPanel.add(new Button("RENT"));
					JPanel tmp = createRentPanelMenu();
					centerPanel.add(tmp, BorderLayout.WEST);
					mainPanel.add(centerPanel, BorderLayout.CENTER);
				}
			}
		});
		String[] moneyCurrent = { "ADF", "ADP", "AED", "AFA", "AFN", "ALL", "AMD", "ANG", "AOA", "AOC", "AOE", "AOK",
				"AOM", "AON", "AOR", "ARA", "ARP", "ARS", "ATS", "AUD", "AWG", "AZM", "AZN", "BAD", "BAM", "BBD", "BDT",
				"BEF", "BGL", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BOV", "BRE", "BRL", "BRR", "BSD", "BTN", "BWP",
				"BYB", "BYN", "BYR", "BZD", "CAD", "CDF", "CHE", "CHF", "CHW", "CLF", "CLP", "CNY", "COP", "COU", "CRC",
				"CSD", "CSK", "CUC", "CUP", "CVE", "CYP", "CZK", "DEM", "DJF", "DKK", "DOP", "DZD", "ECS", "ECV", "EEK",
				"EGP", "ERN", "ESP", "ETB", "EUR", "FIM", "FJD", "FKP", "FRF", "GBP", "GEL", "GHC", "GHP", "GHS", "GIP",
				"GMD", "GNF", "GNS", "GQE", "GRD", "GTQ", "GWP", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "IEP",
				"ILS", "INR", "IQD", "IRR", "ISK", "ITL", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW",
				"KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LTL", "LUF", "LVL", "LYD", "MAD", "MCF", "MDL",
				"MGA", "MGF", "MKD", "MLF", "MMK", "MNT", "MOP", "MRO", "MRU", "MTL", "MUR", "MVR", "MWK", "MXN", "MXP",
				"MXV", "MYR", "MZM", "MZN", "NAD", "NGN", "NIO", "NLG", "NOK", "NPR", "NZD", "OMR", "PAB", "PEI", "PEN",
				"PGK", "PHP", "PKR", "PLN", "PLZ", "PTE", "PYG", "QAR", "ROL", "RON", "RSD", "RUB", "RUR", "RWF", "SAR",
				"SBD", "SCR", "SDD", "SDG", "SDP", "SEK", "SGD", "SHP", "SIT", "SKK", "SLL", "SML", "SOS", "SRD", "SRG",
				"SSP", "STD", "STN", "SVC", "SYP", "SZL", "THB", "TJR", "TJS", "TMM", "TMT", "TND", "TOP", "TPE", "TRL",
				"TRY", "TTD", "TWD", "TZS", "UAH", "UGS", "UGX", "USD", "USN", "USS", "UYI", "UYN", "UYP", "UYU", "UZS",
				"VAL", "VEB", "VEF", "VES", "VND", "VUV", "WST", "XAF", "XAG", "XAU", "XBA", "XBB", "XBC", "XBD", "XBT",
				"XCD", "XDR", "XEU", "XFO", "XFU", "XOF", "XPD", "XPF", "XPT", "XSU", "XTS", "XUA", "XXX", "YER", "YUD",
				"YUM", "YUN", "ZAR", "ZMK", "ZMW", "ZRN", "ZWD", "ZWL", "ZWN", "ZWR" };

		JComboBox jcb = new JComboBox(moneyCurrent);
		jcb.addItemListener(new ItemState());
		jcb.addActionListener(new ItemAction());
		toolbar.add(jcb, BorderLayout.WEST);

		toolbar.add(btnConnex);
		toolbar.add(btnMenuPurchase);
		toolbar.add(btnMenuRent);
		toolbar.add(btnQuit);

	}

	/**
	 * Creer un panel pour l'achat de vehicule ( menu + centre)
	 * 
	 * @return
	 */
	private JPanel createPurchasePanelMenu() {
		JPanel purchasePanelMenu = new JPanel();
		purchasePanelMenu.setLayout(new BoxLayout(purchasePanelMenu, BoxLayout.PAGE_AXIS));

		Button btnDisplayPurchaseVehicles = new Button("Afficher les véhicules");
		Button btnDisplayPurchaseBasket = new Button("Afficher votre panier");

		btnDisplayPurchaseVehicles.addActionListener(e -> {
			System.out.println("Clique sur l'onglet : lister vehicule A");

			centerPanel.setVisible(false);
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout());
			JPanel center = createPanelPurchaseVehicule();
			centerPanel.add(center, BorderLayout.CENTER);
			JPanel tmp = createPurchasePanelMenu();

			centerPanel.add(tmp, BorderLayout.WEST);
			mainPanel.add(centerPanel, BorderLayout.CENTER);

			this.repaint();
			this.revalidate();
		});

		// liste le panier Achat
		btnDisplayPurchaseBasket.addActionListener(e -> {
			System.out.println("Liste panier achat");

			// TODO Auto-generated method stub
			System.out.println("Clique sur l'onglet : lister vehicule A");

			centerPanel.setVisible(false);
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout());
			JPanel center = createPanelBasketPurchaseVehicule();
			centerPanel.add(center, BorderLayout.CENTER);
			JPanel tmp = createPurchasePanelMenu();
			JPanel menuRentBAsket = new JPanel();
			menuRentBAsket.setLayout(new BoxLayout(menuRentBAsket, BoxLayout.Y_AXIS));
			Button btnValide = new Button("Valider");
			// TODO relier le bouton

			JLabel jlblUsername = new JLabel("numCompte");
			JLabel jlblPassword = new JLabel("Password");
			JTextField jtfUsername = new JTextField(15);
			JPasswordField jpfPassword = new JPasswordField();

			JPanel p3 = new JPanel(new GridLayout(2, 1));
			p3.add(jlblUsername);
			p3.add(jlblPassword);

			// On rajoute les champs TextField : login et mdp
			JPanel p4 = new JPanel(new GridLayout(2, 1));
			p4.add(jtfUsername);
			p4.add(jpfPassword);

			// On ajoute p3 et p4 pour crÃ©er la zone de Pop Up
			JPanel p1 = new JPanel();
			p1.add(p3);
			p1.add(p4);

			btnValide.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					int connexionPopUp = JOptionPane.showConfirmDialog(mainPanel, p1, "Connexion",
							JOptionPane.OK_CANCEL_OPTION);
					if (connexionPopUp == JOptionPane.OK_OPTION) {
						// TODO
						System.out.println("Yessss");
						System.out.println(jtfUsername.getText());
						System.out.println(jpfPassword.getPassword());
						if (2 == clientStub.validatePurchaseBasketFront(jtfUsername.getText(),
								String.valueOf(jpfPassword.getPassword()))) {
							System.out.println("Connexion � l'application avec l'utilisateur : " + jtfUsername);
						} else {
							System.out.println("La connexion � l'application a �chou� : Mauvais login ou MDP ");
						}

					} else {
						System.out.println("Connexion annul�");
					}

				}
			});

			menuRentBAsket.add(btnValide);
			centerPanel.add(menuRentBAsket, BorderLayout.EAST);
			centerPanel.add(tmp, BorderLayout.WEST);
			mainPanel.add(centerPanel, BorderLayout.CENTER);

			this.repaint();
			this.revalidate();
		});

		purchasePanelMenu.add(btnDisplayPurchaseVehicles);
		purchasePanelMenu.add(btnDisplayPurchaseBasket);
		return purchasePanelMenu;

	}

	/**
	 * Creer un panel affichant les vehicules dispo à la vente
	 * 
	 * @return
	 */
	private JPanel createPanelPurchaseVehicule() {
		// TODO appeler lst vehicule
		String lst = clientStub.displayVehiclePurchaseFront();

		JPanel tmp = new JPanel();
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.PAGE_AXIS));
		System.out.println("lst : " + lst);
		if (lst.equals("")) {
			return tmp;
		}
		for (String v : lst.split("%")) {
			System.out.println("v : " + v);
			String[] line = v.split(":");
			tmp.add(createItemPurchase(line[0], line[1], line[8], v));
		}
		return tmp;
	}

	/**
	 * Creer un panel item étant un vehicule disponible a la vente
	 * 
	 * @param immat
	 * @param model
	 * @param price
	 * @param other
	 * @return
	 */
	private JPanel createItemPurchase(String immat, String model, String price, String other) {
		JPanel vehicule = new JPanel();
		vehicule.setLayout(new BoxLayout(vehicule, BoxLayout.LINE_AXIS));

		Button btnGetInfoVehicule = new Button("Infos");
		Button btnAddPurchaseBasket = new Button("Ajouter à votre panier");

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
			// TODO creer une pop up
		});
		btnAddPurchaseBasket.addActionListener(e -> {
			int res = clientStub.addPurchaseBasketFront(immat, other);
			System.out.println(res);
			// TODO pop up pour dire ce qu'il se passe
		});

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
			// TODO creer une pop up
			int addVehicleBasket = JOptionPane.showConfirmDialog(mainPanel, res, "AVIS", JOptionPane.CLOSED_OPTION);
		});

		vehicule.add(new Label(model));
		vehicule.add(new Label(price));
		vehicule.add(btnGetInfoVehicule);
		vehicule.add(btnAddPurchaseBasket);

		// TODO mettre mÃ©thode pour creer des boutons ajouter au panier et afficher
		// info
		// vehicule
		return vehicule;
	}

	/**
	 * Creer un panel affichant notre panier des vehicules dispo à la vente
	 * 
	 * @return
	 */
	private JPanel createPanelBasketPurchaseVehicule() {
		// TODO appeler lst vehicule
		String lst = clientStub.displayPurchaseBasketFront();
		JPanel tmp = new JPanel();
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.PAGE_AXIS));
		System.out.println("lst : " + lst);
		if (lst.equals("")) {
			return tmp;
		}
		for (String v : lst.split("%")) {
			System.out.println("v : " + v);
			String[] line = v.split(":");
			tmp.add(createItemPurchaseBasket(line[0], line[1], line[8], v));
		}
		return tmp;
	}

	/**
	 * Creer un panel item étant un vehicule de notre panier disponible a la vente
	 * 
	 * @param immat
	 * @param model
	 * @param price
	 * @param infos
	 * @return
	 */
	private JPanel createItemPurchaseBasket(String immat, String model, String price, String infos) {
		JPanel vehicule = new JPanel();
		vehicule.setLayout(new BoxLayout(vehicule, BoxLayout.LINE_AXIS));

		Button btnGetInfoVehicule = new Button("Infos");
		Button btnRemovePurchaseBasket = new Button("Supprimer");

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
			// TODO creer une pop up
		});
		btnRemovePurchaseBasket.addActionListener(e -> {
			String res = clientStub.removeOnePurchaseBasketFront(immat);
			System.out.println("isRemove : " + res);
			// TODO pop up pour dire ce qu'il se passe
		});

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
			// TODO creer une pop up
			int addVehicleBasket = JOptionPane.showConfirmDialog(mainPanel, res, "AVIS", JOptionPane.CLOSED_OPTION);
		});

		vehicule.add(new Label(model));
		vehicule.add(new Label(price));
		vehicule.add(btnGetInfoVehicule);
		vehicule.add(btnRemovePurchaseBasket);

		// TODO mettre mÃ©thode pour creer des boutons ajouter au panier et afficher
		// info
		// vehicule

		return vehicule;
	}

// ----------------

	/**
	 * Creer un panel pour la location de vehicule ( menu + centre)
	 * 
	 * @return
	 */
	private JPanel createRentPanelMenu() {
		JPanel rentPanelMenu = new JPanel();
		rentPanelMenu.setLayout(new BoxLayout(rentPanelMenu, BoxLayout.PAGE_AXIS));
		// PanelRentBasket

		Button btnDisplayRentVehicles = new Button("Afficher les véhicules");
		Button btnDisplayRentBasket = new Button("Afficher votre panier");// PanelLocationBasket
		Button btnDisplayCurrentlyRentedVehicles = new Button("Véhicules en cours de location");

		btnDisplayRentVehicles.addActionListener(e -> {

			// TODO Auto-generated method stub
			System.out.println("Clique sur l'onglet : lister vehicule A");

			centerPanel.setVisible(false);
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout());
			JPanel center = createPanelRentVehicule();
			centerPanel.add(center, BorderLayout.CENTER);
			JPanel tmp = createRentPanelMenu();
			centerPanel.add(tmp, BorderLayout.WEST);
			mainPanel.add(centerPanel, BorderLayout.CENTER);

			this.repaint();
			this.revalidate();

		});
		btnDisplayRentBasket.addActionListener(e -> {
			System.out.println("Clique sur l'onglet : Lister les vÃ©hicules L");
			// TODO Auto-generated method stub
			System.out.println("Clique sur l'onglet : lister vehicule A");

			centerPanel.setVisible(false);
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout());
			JPanel center = createPanelBasketRentVehicule();
			centerPanel.add(center, BorderLayout.CENTER);
			JPanel tmp = createRentPanelMenu();
			JPanel menuRentBAsket = new JPanel();
			menuRentBAsket.setLayout(new BoxLayout(menuRentBAsket, BoxLayout.Y_AXIS));
			Button btnValide = new Button("Valider");
			// TODO relier le bouton

			JLabel jlblUsername = new JLabel("numCompte");
			JLabel jlblPassword = new JLabel("Password");
			JTextField jtfUsername = new JTextField(15);
			JPasswordField jpfPassword = new JPasswordField();

			JPanel p3 = new JPanel(new GridLayout(2, 1));
			p3.add(jlblUsername);
			p3.add(jlblPassword);

			// On rajoute les champs TextField : login et mdp
			JPanel p4 = new JPanel(new GridLayout(2, 1));
			p4.add(jtfUsername);
			p4.add(jpfPassword);

			// On ajoute p3 et p4 pour crÃ©er la zone de Pop Up
			JPanel p1 = new JPanel();
			p1.add(p3);
			p1.add(p4);
			Front frontTMP = this;
			btnValide.addActionListener(new java.awt.event.ActionListener() {

				@Override
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					int connexionPopUp = JOptionPane.showConfirmDialog(mainPanel, p1, "Connexion",
							JOptionPane.OK_CANCEL_OPTION);
					if (connexionPopUp == JOptionPane.OK_OPTION) {
						// TODO
						if (2 == clientStub.validateRentBasketFront(jtfUsername.getText(),
								String.valueOf(jpfPassword.getPassword()), frontTMP)) {
							System.out
									.println("Connexion � l'application avec l'utilisateur : " + jtfUsername.getText());
						} else {
							System.out.println("La connexion � l'application a �chou� : Mauvais login ou MDP ");
						}

					} else {
						System.out.println("Connexion annul�");
					}

				}
			});

			menuRentBAsket.add(btnValide);
			centerPanel.add(menuRentBAsket, BorderLayout.EAST);
			centerPanel.add(tmp, BorderLayout.WEST);
			mainPanel.add(centerPanel, BorderLayout.CENTER);

			this.repaint();
			this.revalidate();
		});

		btnDisplayCurrentlyRentedVehicles.addActionListener(e -> {
			System.out.println("Clique sur l'onglet : vÃ©hicules en cours de location");

			// TODO Auto-generated method stub
			System.out.println("Clique sur l'onglet : lister vehicule A");

			centerPanel.setVisible(false);
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout());
			JPanel center = createCurrentlyRentedVehicles();
			centerPanel.add(center, BorderLayout.CENTER);
			JPanel tmp = createRentPanelMenu();
			centerPanel.add(tmp, BorderLayout.WEST);
			mainPanel.add(centerPanel, BorderLayout.CENTER);

			this.repaint();
			this.revalidate();
		});

		rentPanelMenu.add(btnDisplayRentVehicles);
		rentPanelMenu.add(btnDisplayRentBasket);
		rentPanelMenu.add(btnDisplayCurrentlyRentedVehicles);
		return rentPanelMenu;
	}

	/**
	 * Creer un panel affichant les vehicules dispo à la location
	 * 
	 * @return
	 */
	private JPanel createPanelRentVehicule() {
		// TODO appeler lst vehicule
		String lst = clientStub.displayVehicleRentedFront();
		JPanel tmp = new JPanel();
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.PAGE_AXIS));
		System.out.println("lst : " + lst);
		for (String v : lst.split("%")) {
			System.out.println("v : " + v);
			String[] line = v.split(":");
			tmp.add(createItemRent(line[0], line[1], line[7], v));
		}
		return tmp;
	}

	/**
	 * Creer un panel item étant un vehicule disponible a la location
	 * 
	 * @param immat
	 * @param model
	 * @param price
	 * @param other
	 * @return
	 */
	private JPanel createItemRent(String immat, String model, String price, String other) {
		JPanel vehicule = new JPanel();
		vehicule.setLayout(new BoxLayout(vehicule, BoxLayout.LINE_AXIS));

		Button btnGetInfoVehicule = new Button("Infos");
		Button btnAddPurchaseBasket = new Button("Ajouter à votre panier");

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
			// TODO creer une pop up
		});
		btnAddPurchaseBasket.addActionListener(e -> {
			int res = clientStub.addRentBasketFront(immat, other);

			System.out.println(res);
			// TODO pop up pour dire ce qu'il se passe
		});

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
			int addVehicleBasket = JOptionPane.showConfirmDialog(mainPanel, res, "AVIS", JOptionPane.CLOSED_OPTION);
		});

		vehicule.add(new Label(model));
		vehicule.add(new Label(price));
		vehicule.add(btnGetInfoVehicule);
		vehicule.add(btnAddPurchaseBasket);

		// TODO mettre mÃ©thode pour creer des boutons ajouter au panier et afficher
		// info
		// vehicule

		return vehicule;
	}

	/**
	 * Creer un panel affichant notre panier des vehicules dispo à la location
	 * 
	 * @return
	 */
	private JPanel createPanelBasketRentVehicule() {
		// TODO appeler lst vehicule
		String lst = clientStub.displayRentBasketFront();
		System.out.println("createPanelBasketRentVehicule lst : " + lst);
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		JPanel tmp = new JPanel();
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.PAGE_AXIS));
		if (lst.equals("")) {
			return tmp;
		}
		System.out.println("lst : " + lst);
		for (String v : lst.split("%")) {
			System.out.println("v : " + v);
			String[] line = v.split(":");
			tmp.add(createItemRentBasket(line[0], line[1], line[7], v));
		}

		panel.add(tmp, BorderLayout.CENTER);
		return panel;
	}

	/**
	 * Creer un panel item étant un vehicule de notre panier disponible a la
	 * location
	 * 
	 * @param immat
	 * @param model
	 * @param price
	 * @param infos
	 * @return
	 */
	private JPanel createItemRentBasket(String immat, String model, String price, String infos) {
		JPanel vehicule = new JPanel();
		vehicule.setLayout(new BoxLayout(vehicule, BoxLayout.LINE_AXIS));

		Button btnGetInfoVehicule = new Button("Infos");
		Button btnRemovePurchaseBasket = new Button("Supprimer");

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
			// TODO creer une pop up
		});
		btnRemovePurchaseBasket.addActionListener(e -> {
			String res = clientStub.removeOneRentBasketFront(immat);

			System.out.println("isRemove : " + res);
			// TODO pop up pour dire ce qu'il se passe
		});

		vehicule.add(new Label(model));
		vehicule.add(new Label(price));
		vehicule.add(btnGetInfoVehicule);
		vehicule.add(btnRemovePurchaseBasket);

		// TODO mettre mÃ©thode pour creer des boutons ajouter au panier et afficher
		// info
		// vehicule

		return vehicule;
	}

	/**
	 * Creer un panel affichant notre panier des vehicules dispo à la location
	 * 
	 * @return
	 */
	private JPanel createCurrentlyRentedVehicles() {
		// TODO appeler lst vehicule
		String lst = clientStub.getCurrentRentalsFront();
		System.out.println("createPanelBasketRentVehicule lst : " + lst);

		JPanel tmp = new JPanel();
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.PAGE_AXIS));
		if (lst.equals("")) {
			return tmp;
		}

		System.out.println("lst : " + lst);
		for (String v : lst.split("%")) {
			System.out.println("v : " + v);
			String[] line = v.split(":");
			// TODO blabla et rendre vehicle
			tmp.add(createItemCurrentRentBasket(line[0], line[1], v));
		}

		return tmp;
	}

	/**
	 * Creer un panel item étant un vehicule de notre panier disponible a la
	 * location
	 * 
	 * @param immat
	 * @param model
	 * @param price
	 * @param infos
	 * @return
	 */
	private JPanel createItemCurrentRentBasket(String immat, String model, String infos) {
		JPanel vehicule = new JPanel();
		vehicule.setLayout(new BoxLayout(vehicule, BoxLayout.LINE_AXIS));

		Button btnGetInfoVehicule = new Button("Infos");
		Button btnReturnCurrentBasket = new Button("Return vehicule");

		btnGetInfoVehicule.addActionListener(e -> {
			String res = clientStub.displayCommentsFront(immat);
			System.out.println(res);
		});
		

		JLabel jlblUsername = new JLabel("commentaire : ");
		JLabel jlblPassword = new JLabel("note : ");
		JTextField jtfUsername = new JTextField(70);
		JTextField jpfPassword = new JTextField(2);

		JPanel p3 = new JPanel(new GridLayout(2, 1));
		p3.add(jlblUsername);
		p3.add(jlblPassword);

		// On rajoute les champs TextField : login et mdp
		JPanel p4 = new JPanel(new GridLayout(2, 1));
		p4.add(jtfUsername);
		p4.add(jpfPassword);

		// On ajoute p3 et p4 pour créer la zone de Pop Up
		JPanel p1 = new JPanel();
		p1.add(p3);
		p1.add(p4);
		btnReturnCurrentBasket.addActionListener(new java.awt.event.ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				int connexionPopUp = JOptionPane.showConfirmDialog(mainPanel, p1, "Connexion",
						JOptionPane.OK_CANCEL_OPTION);
				if (connexionPopUp == JOptionPane.OK_OPTION) {
					// TODO
					if (1 == clientStub.returnVehicle(immat, Integer.valueOf(jpfPassword.getText()),jtfUsername.getText())) {
						System.out
								.println("la transaction s'est bien pass�e va bien");
					} else {
						System.out.println("Impossible de reourner le vehicule");
					}

				} else {
					System.out.println("Connexion annul�e");
				}

			}
		});
		
		vehicule.add(new Label(model));
		vehicule.add(btnGetInfoVehicule);
		vehicule.add(btnReturnCurrentBasket);

		return vehicule;
	}

	private void setUp() {
		this.setVisible(true);

		mainPanel = (JPanel) this.getContentPane();
		mainPanel.setLayout(new BorderLayout());

		// CrÃ©ation de la toolBar
		JToolBar toolbar = new JToolBar();
		createMenu(toolbar);
		createPurchasePanelMenu();
		createRentPanelMenu();

		mainPanel.add(toolbar, BorderLayout.NORTH);

		centerPanel = (JPanel) new JPanel();
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(new Button("PURCHASE"));

		// centerPanel.add(purchasePanelMenu, BorderLayout.WEST);
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		mainPanel.add(new JLabel("RÃ©alisÃ© par J.CROHARE, T.CALONNE et T.JUILLARD "), BorderLayout.SOUTH);

	}

	public static void main(String[] args) throws Exception {

		// Try to set Nimbus look and feel
		UIManager.setLookAndFeel(new NimbusLookAndFeel());

		// Start the demo
		Front myFront = new Front();
		myFront.setUp();

	}
	// Classe interne impl�mentant l'interface ItemListener
    class ItemState implements ItemListener {
        public void itemStateChanged(ItemEvent e) {
            System.out.println("�v�nement d�clench� sur : " + e.getItem());
            clientStub.changeCurrencyFront(e.getItem().toString());
        }
    }

	class ItemAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.out.println("ActionListener : action sur " + jcb.getSelectedItem());
		}
	}
}

