/**
 * IfsCarsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge;

public interface IfsCarsService extends java.rmi.Remote {
    public java.lang.String getInfo(java.lang.String immat) throws java.rmi.RemoteException;
    public int isAvailable(java.lang.String immat) throws java.rmi.RemoteException;
    public boolean isEmployee(java.lang.String name, java.lang.String password) throws java.rmi.RemoteException;
    public boolean purchaseVehicle(java.lang.String immat, java.lang.String nameAccount, java.lang.String login) throws java.rmi.RemoteException;
    public int[] payPurchaseBasket(java.lang.String[] basket, java.lang.String nameAccount, java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public int[] payRentBasket(java.lang.String[] basket, java.lang.String nameAccount, java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public int rentVehicle(java.lang.String imma, java.lang.String nameAccount, java.lang.String login) throws java.rmi.RemoteException;
    public java.lang.String getCurrentRentals(java.lang.String login) throws java.rmi.RemoteException;
    public java.lang.String getComments(java.lang.String imma) throws java.rmi.RemoteException;
    public java.lang.String[] getVehicleInfosTab(java.lang.String immat) throws java.rmi.RemoteException;
    public java.lang.String getSubscribe() throws java.rmi.RemoteException;
    public boolean changeCurrency(java.lang.String newCurrency) throws java.rmi.RemoteException;
    public boolean returnVehicleWithFeedback(java.lang.String login, java.lang.String immat, int grade, java.lang.String comment) throws java.rmi.RemoteException;
    public boolean returnVehicleWithoutFeedback(java.lang.String login, java.lang.String immat) throws java.rmi.RemoteException;
    public java.lang.String getRentableVehicles() throws java.rmi.RemoteException;
    public java.lang.String getPurchasableVehicles() throws java.rmi.RemoteException;
}
