package fr.uge;

public class IfsCarsServiceProxy implements fr.uge.IfsCarsService {
  private String _endpoint = null;
  private fr.uge.IfsCarsService ifsCarsService = null;
  
  public IfsCarsServiceProxy() {
    _initIfsCarsServiceProxy();
  }
  
  public IfsCarsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIfsCarsServiceProxy();
  }
  
  private void _initIfsCarsServiceProxy() {
    try {
      ifsCarsService = (new fr.uge.IfsCarsServiceServiceLocator()).getIfsCarsService();
      if (ifsCarsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ifsCarsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ifsCarsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ifsCarsService != null)
      ((javax.xml.rpc.Stub)ifsCarsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.uge.IfsCarsService getIfsCarsService() {
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService;
  }
  
  public java.lang.String getInfo(java.lang.String immat) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getInfo(immat);
  }
  
  public int isAvailable(java.lang.String immat) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.isAvailable(immat);
  }
  
  public boolean isEmployee(java.lang.String name, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.isEmployee(name, password);
  }
  
  public boolean purchaseVehicle(java.lang.String immat, java.lang.String nameAccount, java.lang.String login) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.purchaseVehicle(immat, nameAccount, login);
  }
  
  public int[] payPurchaseBasket(java.lang.String[] basket, java.lang.String nameAccount, java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.payPurchaseBasket(basket, nameAccount, login, password);
  }
  
  public int[] payRentBasket(java.lang.String[] basket, java.lang.String nameAccount, java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.payRentBasket(basket, nameAccount, login, password);
  }
  
  public int rentVehicle(java.lang.String imma, java.lang.String nameAccount, java.lang.String login) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.rentVehicle(imma, nameAccount, login);
  }
  
  public java.lang.String getCurrentRentals(java.lang.String login) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getCurrentRentals(login);
  }
  
  public java.lang.String getComments(java.lang.String imma) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getComments(imma);
  }
  
  public java.lang.String[] getVehicleInfosTab(java.lang.String immat) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getVehicleInfosTab(immat);
  }
  
  public java.lang.String getSubscribe() throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getSubscribe();
  }
  
  public boolean changeCurrency(java.lang.String newCurrency) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.changeCurrency(newCurrency);
  }
  
  public boolean returnVehicleWithFeedback(java.lang.String login, java.lang.String immat, int grade, java.lang.String comment) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.returnVehicleWithFeedback(login, immat, grade, comment);
  }
  
  public boolean returnVehicleWithoutFeedback(java.lang.String login, java.lang.String immat) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.returnVehicleWithoutFeedback(login, immat);
  }
  
  public java.lang.String getRentableVehicles() throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getRentableVehicles();
  }
  
  public java.lang.String getPurchasableVehicles() throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getPurchasableVehicles();
  }
  
  
}