package fr.uge.observe;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import client.Front;

public class Observateur extends UnicastRemoteObject implements IObservateur {
	private final Front front;

	public Observateur(Front front) throws RemoteException {
		// TODO Auto-generated constructor stub
		this.front = front;
	}
	
	public Observateur() throws RemoteException {
		this(null);
	}

	@Override
	public void notifyChange(String vehicule) throws RemoteException {
		System.out.println("Votre véhicule " + vehicule + " est prêt !");
		if(front != null) {
			front.callNotify("Votre véhicule " + vehicule + " est prêt !");
		}
	}
}
