package fr.uge.observe;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IObserve extends Remote{
	
	public void subscribe(IObservateur observateur, String immat) throws RemoteException;
	
	public void notifyReady(String immat) throws RemoteException;
}
