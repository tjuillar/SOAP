package fr.uge.observe;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IObservateur extends Remote {
	public void notifyChange(String vehicule) throws RemoteException;
}
