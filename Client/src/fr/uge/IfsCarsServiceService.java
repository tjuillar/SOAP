/**
 * IfsCarsServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge;

public interface IfsCarsServiceService extends javax.xml.rpc.Service {
    public java.lang.String getIfsCarsServiceAddress();

    public fr.uge.IfsCarsService getIfsCarsService() throws javax.xml.rpc.ServiceException;

    public fr.uge.IfsCarsService getIfsCarsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
