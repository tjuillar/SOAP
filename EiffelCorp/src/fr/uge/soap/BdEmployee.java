package fr.uge.soap;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.uge.data.EiffelCorp.Employee;
import fr.uge.data.EiffelCorp.IBdEmployee;

public class BdEmployee extends UnicastRemoteObject implements IBdEmployee {
	
	// private List<Employee> employees;
	private HashMap<String, Employee> employees;

	public BdEmployee() throws RemoteException{
		employees = new HashMap<String, Employee>();
	}

	@Override
	public boolean isEmployee(String login, String password) throws RemoteException {
		Employee employee = employees.get(login);
		if (employee == null) {
			return false;
		}
		return employee.password.equals(password);
	}

	public void addEmployee(Employee employee) throws RemoteException {
		employees.put(employee.login, employee);
	}

	@Override 
	public Employee getEmployee(String login) throws RemoteException {
		return employees.get(login);
	}
	
	@Override
	public void addRenting(String login, String immat) throws RemoteException  {
		employees.get(login).addRenting(immat);
	}
	
	public void removeRenting(String login, String immat) throws RemoteException {
		employees.get(login).removeRenting(immat);
	}
}
