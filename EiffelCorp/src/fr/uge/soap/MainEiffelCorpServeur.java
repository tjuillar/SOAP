package fr.uge.soap;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import fr.uge.data.EiffelCorp.Employee;
import fr.uge.data.EiffelCorp.IBdEmployee;;

public class MainEiffelCorpServeur {
	
	static private BdEmployee CreateDataBase() throws RemoteException {
		BdEmployee bdEmployee = new BdEmployee();
		bdEmployee.addEmployee(new Employee("tjuillar", "Thomas", "Juillard", "azerty"));
		bdEmployee.addEmployee(new Employee("tcalonne", "Thomas", "Calonne", "azerty"));
		bdEmployee.addEmployee(new Employee("jcrohare", "Jeanne", "Crohare", "azerty"));
		return bdEmployee;
	}

	public static void main(String[] args) {
		try {
			LocateRegistry.createRegistry(5555);
			IBdEmployee bdEmployee = (IBdEmployee) CreateDataBase();
			Naming.rebind("rmi://localhost:5555/EiffelCorp", bdEmployee);
		} catch (Exception e) {
			System.out.println("Trouble: " + e);
		}
		System.out.println("server eiffelCorp started");
	}
}
