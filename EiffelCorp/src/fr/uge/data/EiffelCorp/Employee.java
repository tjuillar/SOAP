package fr.uge.data.EiffelCorp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Employee implements Serializable{
	
	public final String login;
	private final String firstName;
	private final String lastName;
	public final String password;
	private final List<String> rentings = new ArrayList<String>();
	
	public Employee(String login, String firstName, String lastName, String password) {
		this.login = login;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}
	
	public void addRenting(String immat) {
		rentings.add(immat);
	}
	
	public boolean removeRenting(String immat) {
		boolean res = rentings.remove(immat);
		return res;
	}
	
	public List<String> rentings() {
		return List.copyOf(rentings);
	}
	
}