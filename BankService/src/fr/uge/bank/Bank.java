package fr.uge.bank;

import java.util.HashMap;
public class Bank {
	private final HashMap<String, Account> bank = new HashMap<String, Account>();
	
	public Bank() {
		bank.put("jcrohare", new Account("jcrohare", "c3d4", 50000));
		bank.put("tcalonne", new Account("tcalonne", "b2c3", 52));
		bank.put("tjuillar", new Account("tjuillar", "a1b2", 100000));
	}
	
	/*
	 * @param login Login du client
	 * @param password Mot de passe du client � mentionner pour s'authentifier
	 * @param value Valeur � soustraire au compte bancaire
	 * @return -2 Un des param�tres envoy�s ne correspondait pas aux standards attendus
	 * -1 Login ou mot de passe incorrect 
	 * 0 Solde insuffisant
	 * 1 L'argent a bien �t� d�bit�
	 */
	public int pay(String login, String password, int value) {
		
		if (login == null || password == null || value < 0) {
			return -2;
		}
		
		var account = bank.get(login);
		if (account == null || !password.equals(account.getPassword())) {
			return -1;
		}
		
		return (account.dec(value))?1:0;
	}
	
}
