package fr.uge.bank;

import java.util.Objects;

public class Account {
	private int amount;
	private final String login;
	private final String password;
	public Account(String login, String password, int amount) {
		this.login = Objects.requireNonNull(login);
		this.password = Objects.requireNonNull(password);
		this.amount = amount;
	}
	
	public Account(String login, String password) {
		this.login = Objects.requireNonNull(login);
		this.password = Objects.requireNonNull(password);
		this.amount = 0;
	}
	
	/*

	 * @return le login
	 * 
	 */
	public String getLogin() {
		return login;
	}
	
	
	/*

	 * @return le mot de passe
	 * 
	 */
	public String getPassword() {
		return password;
	}
	
	/*

	 * @return le solde disponible sur le compte
	 * 
	 */
	public int getAmount() {
		return amount;
	}
	
	/*
	 * @param value Valeur � ajouter au solde
	 * @return false si la valeur est n�gative, true si la valeur est positive et qu'elle a bien �t� ajout� au solde.
	 * 
	 */
	public boolean inc(int value) {
		if (value <= 0) {
			return false;
		}
		amount += value;
		return true;
	}
	
	/*
	 * @param value Valeur � enlever au solde
	 * @return false si la valeur est n�gative, true si la valeur est positive et qu'elle a bien �t� soustraite du solde.
	 * 
	 */
	public boolean dec(int value) {
		if (value > amount || value < 0) {
			return false;
		}
		amount -= value;
		return true;
	}
	
	/*
	 * @param value Valeur � analyser
	 * @return true si le solde est suffisant, false sinon.
	 * 
	 */
	public boolean asEnoughBalance(int value) {
		return value > amount;
	}
}
