Jeanne CROHARE
Thomas CALONNE
Thomas JUILLARD
                                        				
Le 29 novembre 2020



# README

Les documentations sont accessibles depuis le répertoire racine du projet: 
 - Documentation utilisateur : **./utilisateur/manuel_utilisateur**
 - Documentation développeur : **./utilisateur/manuel_developpeur**


## I. Installation et démarrage de l’application
Pour l’installation et le lancement de l’application vous pouvez vous référer au chapitre correspondant dans le manuel utilisateur mis à disposition ici : 
**./utilisateur/manuel_utilisateur/manuel_utilisateur.pdf**

## II. Les manuels
Le chemin pour accéder au manuel du développeur est : 
**./doc/developpeurs/manuel_developpeur.pdf**

Le chemin pour accéder au manuel utilisateur est : 
**./doc/utilisateurs/manuel_utilisateur.pdf**

## III. Les sources de l’applications
Le chemin pour accéder à l'ensemble des sources du projet est : **./** 


