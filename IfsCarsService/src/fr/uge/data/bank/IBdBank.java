package fr.uge.data.bank;

import java.rmi.Remote;
import java.rmi.RemoteException;

/***
 * Interface qui offre des services RMI pour la banque
 * @author tJuillard tCalonne jCrohare
 *
 */
public interface IBdBank extends Remote{
	
	public boolean pay(String debitNum, String creditNum, int money) throws RemoteException;
	public int getSoldeAccount(String NameAccount) throws RemoteException;
}
