package fr.uge.data.bank;

import java.io.Serializable;
/***
 * Classe repr�sentant un compte en banque
 * @author tJuillard tCalonne jCrohare
 *
 */
public class Account implements Serializable {
	public final String name;
	public final String accountNum;
	private int money;
	

	public Account(String name, String accountNum,int money) {
		this.name = name;
		this.accountNum = accountNum;
		this.money = money;
	}
	
	public Account(String name, String accountNum) {
		this(name, accountNum, 0);
	}	
	

	/**
	 * M�thode permettant de rajouter de l'argent sur le compte
	 * @param money
	 */
	public void credited(int money) {
		this.money += money;
	}
	
	/**
	 * M�thode permettant de retirer de l'argent sur le compte
	 * @param money
	 */
	public void debited(int money) {
		this.money -= money;
	}
	
	/**
	 * M�thode permetant de r�cup�rer la money du compte. C'est un int contenant la monnaie du compte
	 * @return 
	 */
	public int getMoney() {
		return money;
		
	}
	
	@Override
	public String toString() {
		return name + " " + accountNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNum == null) ? 0 : accountNum.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountNum == null) {
			if (other.accountNum != null)
				return false;
		} else if (!accountNum.equals(other.accountNum))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	
	
}