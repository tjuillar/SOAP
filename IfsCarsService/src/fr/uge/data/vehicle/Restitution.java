package fr.uge.data.vehicle;

import java.io.Serializable;
import java.util.Date;
/**
 * Cette classe est n�cessaire pour rendre un v�hicule qui a �t� louer. Elle permet �galement de mettre une note et un commentaire.
 * @author tJuillard tCalonne JCrohare
 *
 */
public class Restitution implements Serializable {
	public final Integer grade;
	public final String comment;
	public final String loginEmployee;
	public final Date date;

	public Restitution(Integer grade, String comment, String loginEmployee, Date date) {
		this.grade = grade;
		this.comment = comment;
		this.loginEmployee = loginEmployee;
		this.date = date;
	}

	public Restitution(Integer grade, String comment, String loginEmployee) {
		this(grade, comment, loginEmployee, new Date());
	}

	public Restitution(String loginEmployee) {
		this(null, null, loginEmployee, new Date());
	}
	
	@Override
	public String toString() {
		
		return "Note = " + grade + " / Commentaire = " + comment + " / Employee = " + loginEmployee + " / Date = " + date.toString();
	}

}
