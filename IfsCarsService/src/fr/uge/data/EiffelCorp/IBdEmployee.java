package fr.uge.data.EiffelCorp;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBdEmployee extends Remote {
	
	public boolean isEmployee(String lastName, String firstName) throws RemoteException;
	public Employee getEmployee(String login) throws RemoteException;
	public void addRenting(String login, String immat) throws RemoteException;
	public void removeRenting(String login, String immat) throws RemoteException;
}
