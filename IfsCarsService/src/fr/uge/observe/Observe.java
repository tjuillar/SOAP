package fr.uge.observe;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Observe extends UnicastRemoteObject implements IObserve {
	
	public Observe() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}


	// immat en fonction des observer
	private final HashMap<String, LinkedList<IObservateur>> obs = new HashMap<String, LinkedList<IObservateur>>();

	/**
	 * M�thode qui ajoute un v�hicule pr�t dans la liste qui a pour cl� "login"
	 * 
	 * @param login le login de l'employ�
	 * @param immat l'immatriculation du v�hicule ajout�
	 */
	@Override
	public void subscribe(IObservateur observateur, String immat) {
		LinkedList<IObservateur> l = obs.getOrDefault(immat, new LinkedList<IObservateur>());
		l.add(observateur);
		obs.put(immat, l);
		System.out.println("SUBSCRIBE");
	}

	/**
	 * Methode qui v�rifie s'il y a un v�hicule dans la liste qui a pour cl� "login"
	 * et supprime le v�hicule s'il existe.
	 * 
	 * @param login le login de l'employ�
	 * @return immat l'immatriculation du v�hicule pr�t ou null s'il n'y a aucun
	 *         v�hicule de pr�t
	 */
	@Override
	public void notifyReady(String immat) throws RemoteException {
		System.out.println("NOTIFYYY");
		try {
			IObservateur observateur =  obs.getOrDefault(immat, new LinkedList<IObservateur>()).removeFirst();
			observateur.notifyChange(immat);
		} catch (NoSuchElementException e) {
			return;
		}
		
	}

}
