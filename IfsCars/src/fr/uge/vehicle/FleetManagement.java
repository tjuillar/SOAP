package fr.uge.vehicle;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.uge.data.vehicle.IFleetManagement;
import fr.uge.data.vehicle.Restitution;
import fr.uge.data.vehicle.Vehicle;

/**
 * La classe de la gestion du parc de v�hicule.
 * 
 * @author tJuillard tCalonne jCrohare
 */
public class FleetManagement extends UnicastRemoteObject implements IFleetManagement {

	protected FleetManagement() throws RemoteException {
		super();
	}

	private final Map<String, Vehicle> vehicles = new HashMap<String, Vehicle>(); // Donne la liste des véhicules

	/**
	 * 
	 * @return une copie de la liste de l'ensemble des véhicules
	 */
	@Override
	public List<Vehicle> vehiclesList()  throws RemoteException{
		var res = List.copyOf(vehicles.values());
		return res;
	}

	/**
	 * 
	 * @param imma
	 * @return une copie de la liste des restitutions de la voiture
	 */
	@Override
	public List<Restitution> getRestitutions(String imma) throws RemoteException {
		return (vehicles.getOrDefault(imma, null) != null) ? vehicles.get(imma).getRestitutions()
				: new ArrayList<Restitution>();
	}

	//TODO remove
	public void addVehicle(Vehicle v) throws RemoteException {
		vehicles.put(v.immat, new Vehicle(v.immat, v.brand, v.type, v.places, v.getIsRentedNow(), v.getNbRented(), v.getIsPurshasedNow(), v.rentPrice, v.purchasePrice, v.getRestitutions()));
	}

	/***
	 * Fonction permettant de supprimer un v�hicule
	 * 
	 * @param imma l'imatriculation d'un v�hicule � supprimer
	 */
	private boolean deleteVehicle(String imma) throws RemoteException {
		return null != vehicles.remove(imma);
	}

	/***
	 * Fonction permettant de r�aliser l'achat d'un v�hicule. A l'achat, le v�hicule
	 * est supprim� de la base.
	 * 
	 * @param imma num�ro d'immatriculation d'un v�hicule
	 */
	@Override
	public boolean purchaseVehicle(String imma)  throws RemoteException{
		if(deleteVehicle(imma)) {
			System.out.println("Le v�hicule immatricul� : " + imma + " a �t� achet�");
			return true;
		}
		return false;
	}

	/*
	 * @param String imma: Immatricule du v�hicule concern�
	 * 
	 * @param String login: Login de l'employ� concern�
	 * 
	 * @return -1 si le vehicule n'est pas trouv� dans la base, 0 si l'employ� est
	 * plac� en file d'attente et 1 si la location prend place imm�diatement
	 */
	@Override
	public int rentVehicle(String imma, String login)  throws RemoteException{
		if (!vehicles.keySet().contains(imma)) {
			return -1;
		}
		var vehicle = vehicles.get(imma);
		return (vehicle.rentVehicle(login)) ? 1 : 0;
	}

	/*
	 * @param String imma: Immatriculation du v�hicule concern�
	 * 
	 * @param String loginEmployee: l'identifiant de l'employ�
	 * 
	 * @param Integer grade: Evaluation de la restitution
	 * 
	 * @param String comment: Commentaire associ� � l'�valuation
	 * 
	 * @return false si un probl�me est survenu (si v�hicule pas trouv� dans la
	 * base, si personne ne loue le v�hicule actuellement, si le login mentionn� ne
	 * correspond pas au login de l'employ� qui loue le v�hicule actuellement), true
	 * s'il a bien �t� rendu
	 */
	@Override
	public boolean returnVehicleWithFeedback(String imma, String login, Integer grade, String comment) throws RemoteException {

		if (!vehicles.keySet().contains(imma)) {
			return false;
		}
		var vehicle = vehicles.get(imma);
		boolean res = vehicle.returnVehiculeWithFeedback(login, grade, comment);
		
		
		return res;
	}

	/**
	 * 
	 */
	@Override
	public Vehicle getVehicle(String imma) throws RemoteException {
		return vehicles.get(imma);
	}

	@Override
	public boolean renturnVehicleWithoutFeedback(String imma, String login) throws RemoteException {
		if (!vehicles.keySet().contains(imma)) {
			return false;
		}
		var vehicle = vehicles.get(imma);
		return vehicle.returnVehiculeWithoutFeedback(login);
	}
	
	
	/**
	 * @param immat
	 * @return 0 si le v�hicule n'a pas �t� trouv�
	 * 1 si le v�hicule peut �tre � la fois lou� et achet�
	 * 2 si le v�hicule ne peut �tre que lou�
	 * 3 si le v�hicle a �t� achet�
	 */
	@Override
	public int vehiculeIsAvailable(String imma) throws RemoteException {
		Vehicle vehicle = vehicles.get(imma);
		if (vehicle == null) {
			return 0;
		}
		
		if (!vehicle.canBeRent()) {
			return 3;
		}
		
		if (vehicle.canBePurchase()) {
			return 1;
		}
		
		return 2;
	}


	/*
	 * @return Un tableau de string contenant les informations du v�hicule
	 * 0 : immat | 1 : brand | 2 : type | 3 : places | 4 : is currently rented | 5 : is purchased 
	 * 6 : nb rented | 7 : rent price | 8 : purchase price | 9 : restitutions 
	 */
	@Override
	public String[] infoVehicleTab(String imma) throws RemoteException {
		Vehicle vehicle = vehicles.get(imma);
		if (vehicle == null) {
			return null;
		}	
		return vehicle.infoVehicleTab();
	}


}
