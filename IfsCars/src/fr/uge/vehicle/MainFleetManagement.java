package fr.uge.vehicle;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import fr.uge.data.vehicle.IFleetManagement;
import fr.uge.data.vehicle.Vehicle;

/**
 * Classe main permettant de lancer une gestion de parc de v�hicules.
 * 
 * @author tJuillard tCalonne JCrohare
 */
public class MainFleetManagement {

	static private FleetManagement CreateDataBase() throws RemoteException {
		FleetManagement bdVehicle = new FleetManagement();
		var v = new Vehicle("zodk8oz99", "twingo", "diesel", 2, false, 0, false, 30, 5550);
		v.addComments("tjuillar", 9,
				"Superbe expérience, je conseille fortement ce véhicule. Mon petit ami et moi-même avons adoré le coffre spacieux. La poussette rentre largement à l'intérieur.");

		v.addComments("toto", 1,
				"Horrible ! La climatisation ne fait que du chaud et sent la chaussette pourrie. Il y a des tâches sur les sièges, il y a de la mayonnaise sur le volant et un fromage moisi dans la boite à gants !! Attention arnaque ! ");
		v.addComments("jeanne", 7,
				"Le siège passager était très confortable, j'ai bien dormi pendant toute la durée du trajet. J'ai pas le permis mais mon homme m'a fait conduire sur un parking et j'ai calé seulement 5 fois ! Je suis très satisfaite par cette location ! ^^ ");
		bdVehicle.addVehicle(v);
		bdVehicle.addVehicle(new Vehicle("687930606", "bugatti", "escence", 2, false, 4, false, 100, 10000));
		bdVehicle.addVehicle(new Vehicle("toto", "peugeot", "electrique", 5, false, 0, false, 50, 12542));
		bdVehicle.addVehicle(new Vehicle("toaj526", "toyota", "diesel", 5, false, 0, false, 250, 10));

		return bdVehicle;
	}

	public static void main(String[] args) {
		try {
			LocateRegistry.createRegistry(8888);
			IFleetManagement bdVehicule = (IFleetManagement) CreateDataBase();
			Naming.rebind("rmi://localhost:8888/vehicle", bdVehicule);
		} catch (Exception e) {
			System.out.println("Trouble: " + e);
		}
		System.out.println("server MainFleetManagement started");
	}
}
