package fr.uge.data.vehicle;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface IFleetManagement extends Remote {
	
	/**
	 * 
	 * @return une copie de la liste de l'ensemble des véhicules
	 */
	public List<Vehicle> vehiclesList() throws RemoteException;

	/**
	 * 
	 * @param imma
	 * @return une copie de la liste des restitutions de la voiture
	 */
	public List<Restitution> getRestitutions(String imma) throws RemoteException;
	
	/*
	 * @param String imma: Immatricule du v�hicule concern�
	 * @param String login: Login de l'employ� concern�
	 * @return -1 si le vehicule n'est pas trouv� dans la base, 0 si l'employ� est plac� en file d'attente 
	 * et 1 si la location prend place imm�diatement
	 */
	public int rentVehicle(String imma, String login) throws RemoteException;
	
	/***
	 * 
	 * @param imma la plaque d'immatriculation du v�hicule
	 * @param login de la personne qui veut acheter le v�hicule
	 * @return /TODO
	 */
	public boolean purchaseVehicle(String imma) throws RemoteException;
	
	/*
	 * @param String imma: Immatriculation du v�hicule concern�
	 * @param String loginEmployee: l'identifiant de l'employ� 
	 * @param Integer grade: Evaluation de la restitution
	 * @param String comment: Commentaire associ� � l'�valuation
	 * @return false si un probl�me est survenu (si v�hicule pas trouv� dans la base, 
	 * si personne ne loue le v�hicule actuellement, 
	 * si le login mentionn� ne correspond pas au login de l'employ� qui loue le v�hicule actuellement),
	 *  true s'il a bien �t� rendu
	 */
	public boolean returnVehicleWithFeedback(String imma, String login, Integer grade, String comment) throws RemoteException;
	
	/*
	 * @param String imma: Immatriculation du v�hicule concern�
	 * @param String loginEmployee: l'identifiant de l'employ� 
	 * @return false si un probl�me est survenu (si v�hicule pas trouv� dans la base, 
	 * si personne ne loue le v�hicule actuellement, 
	 * si le login mentionn� ne correspond pas au login de l'employ� qui loue le v�hicule actuellement),
	 *  true s'il a bien �t� rendu
	 */
	public boolean renturnVehicleWithoutFeedback(String imma, String login) throws RemoteException;
	
	/**
	 * 
	 * @param imma numéro d'immatriculation unique du véhicule 
	 * @return Un Optionnal contenant le véhicule ou Optional.Empty() si aucune plaque d'immatriculation correspond à ce véhicule.
	 * @throws RemoteException
	 */
	public Vehicle getVehicle(String imma) throws RemoteException;
	
	
	/**
	 * @param immat
	 * @return 0 si le v�hicule n'a pas �t� trouv�
	 * 1 si le v�hicule peut �tre � la fois lou� et achet�
	 * 2 si le v�hicule ne peut �tre que lou� 
	 */
	public int vehiculeIsAvailable(String imma) throws RemoteException;
	
	/*
	* @return Un tableau de string contenant les informations du v�hicule
	* 0 : immat | 1 : brand | 2 : type | 3 : places | 4 : is currently rented | 5 : is purchased 
	* 6 : nb rented | 7 : rent price | 8 : purchase price | 9 : restitutions 
	*/
	public String[] infoVehicleTab(String imma) throws RemoteException;

}
