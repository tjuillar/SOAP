package fr.uge.data.vehicle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Cette classe d�fnie un v�hicule
 * 
 * @author tJuillard tCalonne JCrohare
 *
 */
public class Vehicle implements Serializable {
	public final String immat;
	public final int rentPrice;
	public final int purchasePrice;
	public final String brand;
	public final String type;
	public final int places;
	
	private boolean isRentedNow;
	private boolean isPurchasedNow;
	private String currentlyRenting;
	private String currentlyPurchasing;
	private int nbRented;

	/**
	 * La liste des personnes qui ont déjà louer les voitures avec éventuellement
	 * les notes qui sont rattachées aux véhicules
	 */
	private List<Restitution> restitutionList;

	/**
	 * La liste d'attente pour les employés souhaitant louer le véhicule
	 */
	private List<String> waitingList = new ArrayList<String>();

	public Vehicle(String immat, String brand, String type, int places, boolean isRented, int nbRented, boolean isPuschased, int rentPrice , int purchasePrice, List<Restitution> restitutionList) {
		this.immat = immat;
		this.brand = brand;
		this.type = type;
		this.places = places;
		this.isRentedNow = isRented;
		this.isPurchasedNow = isPuschased;
		this.nbRented = nbRented;
		this.rentPrice = rentPrice;
		this.purchasePrice = purchasePrice;
		this.restitutionList = new ArrayList<Restitution>();
	}
	
	public Vehicle(String immat, String brand, String type, int places, boolean isRented, int nbRented, boolean isPuschased, int rentPrice , int purchasePrice) {
		this(immat, brand, type, places, isRented, nbRented, isPuschased, rentPrice, purchasePrice, new ArrayList<Restitution>());
	}
	
	public boolean canBePurchase() {
		return nbRented > 0 && !isPurchasedNow && !isRentedNow;
	}
	
	public boolean canBeRent() {
		return !isPurchasedNow;
	}

	/**
	 * 
	 * @return true si le véhicule a déjà été loué une fois, false sinon
	 */
	public boolean isRentedOnce() {
		return nbRented > 0;
	}

	/**
	 * 
	 * @return true si le véhicule est actuellement loué et false sinon
	 */
	public boolean getIsRentedNow() {
		return isRentedNow;
	}

	/**
	 * 
	 * @return true si le véhicule est actuellement vendu et false sinon
	 */
	public boolean getIsPurshasedNow() {
		return isPurchasedNow;

	}

	/**
	 * 
	 * @return une copie de la liste des restitutions
	 */
	public List<Restitution> getRestitutions() {
		return List.copyOf(restitutionList);
	}

	public int getNbRented() {
		return nbRented;
	}
	
	public String getCurrentlyRenting() {
		return currentlyRenting;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((immat == null) ? 0 : immat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (immat == null) {
			if (other.immat != null)
				return false;
		} else if (!immat.equals(other.immat))
			return false;
		return true;
	}

	/**
	 * @param String  loginEmployee: l'identifiant de l'employ�
	 * @param Integer grade: Evaluation de la restitution
	 * @param String  comment: Commentaire associ� � l'�valuation
	 * @return Un bool�en. Si la m�thode renvoie true, tout s'est bien pass� et la
	 *         fiche du v�hicule a �t� mise � jour. Si elle renvoie false, alors
	 *         soit le v�hicule n'est pas lou� actuellement ou le login mentionn� ne
	 *         correspond pas au login de l'employ� qui loue le v�hicule.
	 */
	public boolean returnVehiculeWithFeedback(String loginEmployee, Integer grade, String comment) {
		if (returnVehiculeWithoutFeedback(loginEmployee) == true) {
			restitutionList.add(new Restitution(grade, comment, loginEmployee));
			return true;
		}
		return false;
	}
	
	public boolean returnVehiculeWithoutFeedback(String loginEmployee) {
		if (!isRentedNow) {
			return false;
		}
		if (!currentlyRenting.equals(loginEmployee)) {
			return false;
		}

		if (waitingList.size() != 0) {
			isRentedNow = true; // Est d�j� sur true normalement
			nbRented++;
			currentlyRenting = waitingList.remove(0);
		} else {
			isRentedNow = false;
			currentlyRenting = null;
		}
		return true;
	}
	
	public void addComments(String loginEmployee, Integer grade, String comment) {
		restitutionList.add(new Restitution(grade, comment, loginEmployee));
	}

	/**
	 * @param String login - l'identifiant de l'employ�
	 * @return true si la location prend place d�s maintenant, false si l'employ�
	 *         est plac� en file d'attente
	 */
	public boolean rentVehicle(String login) {

		if (!isRentedNow) {
			currentlyRenting = login;
			nbRented++;
			isRentedNow = true;
			return true;
		}

		waitingList.add(Objects.requireNonNull(login));
		return false;
	}
	/***
	 * 
	 * @param imma la plaque d'immatriculation du v�hicule
	 * @param login de la personne qui veut acheter le v�hicule
	 * @return /TODO
	 */
	public boolean purchaseVehicle(String imma, String login) {
		if (!isPurchasedNow) {
			currentlyPurchasing = login;
			isPurchasedNow = true;
			return true;
		}
		return false;
	}
	

	/**
	 * @return Les informations sous forme d'une String du vehicule. M�thode
	 *         impl�ment�e pour les tests.
	 */
	public String infos() {
		return "Vehicle\n\t* immat: " + immat + "\n\t* brand: " + brand + "\n\t* type: " + type + "\n\t* places: "
				+ places + "\n\t* is rented now: " + ((isRentedNow) ? "yes by " + currentlyRenting : "no")
				+ "\n\t* has been rented: " + nbRented + " time(s)." + "\n\t* restitutions: "
				+ restitutionList.toString() + "\n\t* waiting list: " + waitingList.toString();
	}
	
	/*
	 * @return Un tableau de string contenant les informations du v�hicule
	 * 0 : immat | 1 : brand | 2 : type | 3 : places | 4 : is currently rented | 5 : is purchased 
	 * 6 : nb rented | 7 : rent price | 8 : purchase price | 9 : restitutions 
	 */
	public String[] infoVehicleTab() {
		String[] res = new String[10];
		res[0] = immat;
		res[1] = brand;
		res[2] = type;
		res[3] = Integer.toString(places);
		res[4] = (isRentedNow)? "yes":"no";
		res[5] = (isPurchasedNow)? "yes":"no";
		res[6] = Integer.toString(nbRented);
		res[7] = Integer.toString(rentPrice);
		res[8] = Integer.toString(purchasePrice);
		if (restitutionList.size() == 0) {
			res[9] = " ";
		}
		else {
			StringBuilder sb = new StringBuilder();
			for (Restitution r : restitutionList)
			{
			    sb.append(r.toString());
			    sb.append(",\n\t");
			}
			res[9] = sb.toString();
		}
		return res;
	}

}
